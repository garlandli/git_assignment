//Comments here
//Code to calculate factorial of a number
// This is commented code lol
import java.util.Scanner;
//The main class 
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
//And here
 //Test end case
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
//And here`
//Actual logic for calculating factorial
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
